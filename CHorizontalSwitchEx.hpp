
#ifndef _CHORIZONTALSWITCHEX_HPP_8DKFUR
#define _CHORIZONTALSWITCHEX_HPP_8DKFUR

#include <vstgui.h>

/** 
 This class extend CVerticalSwitch to put start and end point restriction. 
 You need to use it when your image's mouse start point different from 0 
    and end point is different from one image height size.
 */
class CHorizontalSwitchEx : public CHorizontalSwitch {
public:

    /** 
     iMaxPositions is ending point of mouse positions
     startPoint is starting point of mouse positions
     reverse is reverse value 0 to 1, 1 to 0, 0.25 to 0.75 e.g...
     */
    CHorizontalSwitchEx(const CRect& size, CControlListener* listener, long tag, long subPixmaps,
                      CCoord heightOfOneImage, long heightOfMousableImage, long iMaxPositions, CBitmap* background,
                      long startPoint = 0, const CPoint& offset = CPoint (0, 0));
    
    virtual ~CHorizontalSwitchEx() {}
    
    virtual CMouseEventResult onMouseDown (CPoint& where, const CButtonState& buttons);
    virtual CMouseEventResult onMouseMoved (CPoint& where, const CButtonState& buttons);

protected:
    long m_heightOfMousableImage;
    long m_startPoint;
    long _iMaxPositions;
    double m_myCoef;
    float _reverse;
};

inline CHorizontalSwitchEx::CHorizontalSwitchEx (const CRect& size, CControlListener* listener, long tag,
                                             long subPixmaps, CCoord heightOfOneImage, long heightOfMousableImage,
                                             long iMaxPositions, CBitmap* background, long startPoint,
                                             const CPoint& offset)
: CHorizontalSwitch(size, listener, tag, subPixmaps, heightOfOneImage, iMaxPositions, background, offset),
m_heightOfMousableImage(heightOfMousableImage), m_startPoint(startPoint), _iMaxPositions(iMaxPositions)
{ }

inline CMouseEventResult CHorizontalSwitchEx::onMouseDown(CPoint& where, const CButtonState& buttons)
{
    if (!(buttons & kLButton))
        return kMouseEventNotHandled;
    
    m_myCoef = (double)m_heightOfMousableImage / (double)_iMaxPositions;
    
    beginEdit ();
    
    if (checkDefaultValue (buttons))
    {
        endEdit ();
        return kMouseDownEventHandledButDontNeedMovedOrUpEvents;
    }
    
    return onMouseMoved (where, buttons);
}

inline CMouseEventResult CHorizontalSwitchEx::onMouseMoved(CPoint& where, const CButtonState& buttons)
{
    if (buttons & kLButton)
    {
        float norm = (int32_t)((where.h - size.left - m_startPoint) / m_myCoef) / (float)(_iMaxPositions - 1);
        value = getMin () + norm * (getMax () - getMin ());
        
        if (value > getMax ())
            value = getMax ();
        else if (value < getMin ())
            value = getMin ();

        if (isDirty ())
        {
            if (listener)
                listener->valueChanged (this);
            invalid ();
        }
    }
    return kMouseEventHandled;
}

#endif // _CHORIZONTALSWITCHEX_HPP_8DKFUR

