
#ifndef _CHYPERLINKLABER_HPP_8DJ63S
#define _CHYPERLINKLABER_HPP_8DJ63S

#include <vstgui.h>

class CHyperlinkLabel : public CTextLabel {
    
public:
    
    CHyperlinkLabel(const CRect& size, const char* txt = NULL, const char* link = NULL,
                     CBitmap* background = NULL, const long style = 0)
    : CTextLabel(size, txt, background, style), _link(link)
    {}
    
    virtual CMouseEventResult onMouseDown (CPoint& where, const CButtonState& buttons)
    {
#ifdef VKWIN
        ::ShellExecuteA(NULL, "open", _link.c_str(), NULL, NULL, SW_SHOWNORMAL);
#else
        std::string command = std::string("open ") +  _link;
        system(command.c_str());
#endif

        return kMouseEventHandled;
    }

private:
    
    std::string _link;

};

#endif // _CHYPERLINKLABER_HPP_8DJ63S
